NAME=slapd-supann
VERSION=`git describe | tr - . | cut -c2-`
FULLNAME=$(NAME)-$(VERSION)

LIBDIR = $(DESTDIR)/usr/lib/slapd-supann
BINDIR = $(DESTDIR)/usr/bin
SBINDIR = $(DESTDIR)/usr/sbin
ETCDIR = $(DESTDIR)/etc
SHAREDIR = $(DESTDIR)/usr/share/slapd-supann
MANDIR = $(DESTDIR)/usr/share/man

all:

install:
	install -o root -g root -m 0755 slapd-supann $(BINDIR)
	install -o root -g root -m 0644 slapd-supann.1 $(MANDIR)/man1
	install -o root -g root -m 0755 -d $(LIBDIR)
	install -o root -g root -m 0755 lib/* $(LIBDIR)
	chmod 0644 $(LIBDIR)/*.help
	install -o root -g root -m 0755 -d $(SHAREDIR)
	install -o root -g root -m 0755 share/* $(SHAREDIR)

uninstall:
	rm $(BINDIR)/slapd-supann
	rm $(MANDIR)/man1/slapd-supann.1
	rm -rf $(LIBDIR) $(SHAREDIR)

# eobuilder targets -- see http://repos.entrouvert.org/eobuilder.git/tree/README.rst
dist-bzip2:
	rm -rf build dist
	mkdir -p build/$(FULLNAME) sdist
	for i in *; do \
		if [ "$$i" != "build" ]; then \
			cp -R "$$i" build/$(NAME)-$(VERSION); \
		fi; \
	done
	cd build && tar cfj ../sdist/$(FULLNAME).tar.bz2 .
	rm -rf build

clean:
	rm -rf sdist build

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(FULLNAME))

package:
	rm -rf debian sdist
	make dist-bzip2
	git checkout debian debian
	cd sdist; mv $(FULLNAME).tar.bz2 $(NAME)_$(VERSION).orig.tar.bz2; tar xvjf $(NAME)_$(VERSION).orig.tar.bz2
	cp -R debian sdist/$(FULLNAME)/
	cd sdist/$(FULLNAME); dch -v $(VERSION)-1 "New upstream release"; dpkg-buildpackage -uc -us
	rm -rf sdist/$(FULLNAME)
	git rm -r --cached debian
	rm -rf debian

update-dev:
	rsync -avz --delete lib/ ldap1-psl.dev:/usr/lib/slapd-supann/
	rsync -avz --delete share/ ldap1-psl.dev:/usr/share/slapd-supann/

take-dev:
	rsync -avz --delete ldap1-psl.dev:/usr/lib/slapd-supann/ lib/
	rsync -avz --delete ldap1-psl.dev:/usr/share/slapd-supann/ share/
