#!/bin/sh
# initial configuration of slapd

set -e
LDIFDIR=/usr/share/slapd-supann
SERVICE="/usr/sbin/service slapd"

echo ""
echo "  *************"
echo "  *           *   La configuration et toutes les données"
echo "  * ATTENTION *   de l'annuaire LDAP vont être définitivement"
echo "  *           *   effacées. Avez-vous fait un backup ?"
echo "  *************"

echo ""
echo "Confirmez la MISE A ZÉRO COMPLÈTE de l'annuaire LDAP."
echo ""
echo -n "Tapez oui en toutes lettres : "
read ok
if [ "x$ok" != "xoui" ]; then
	exit 3
fi

${SERVICE} stop || true

echo -n "Effacement de la configuration"
rm -rf /etc/ldap/slapd.d/*
echo -n " et des données .."
rm -rf /var/lib/ldap/*
echo "ok"

if ! grep "^\s*profile\s\+config\s*$" /etc/ldapvi.conf > /dev/null 2>&1; then
	echo "(ajout du 'profile config' dans /etc/ldapvi.conf)"
	cat << EOLDAPVI >> /etc/ldapvi.conf

profile config
host: ldapi://
sasl-mech: EXTERNAL
base: cn=config

EOLDAPVI
fi

mkdir -p /etc/ldap/slapd.d
mkdir /var/lib/ldap/config-accesslog/ /var/lib/ldap/meta/

echo "Installation de la nouvelle configuration .. "
slapadd -n0 -F/etc/ldap/slapd.d -l${LDIFDIR}/config.ldif
echo "ok"

echo "Installation des schémas .. "
slapadd -n0 -F/etc/ldap/slapd.d -l/etc/ldap/schema/core.ldif
slapadd -n0 -F/etc/ldap/slapd.d -l/etc/ldap/schema/cosine.ldif
slapadd -n0 -F/etc/ldap/slapd.d -l/etc/ldap/schema/inetorgperson.ldif
slapadd -n0 -F/etc/ldap/slapd.d -l${LDIFDIR}/supann-2009.ldif
slapadd -n0 -F/etc/ldap/slapd.d -l${LDIFDIR}/eduperson.ldif
slapadd -n0 -F/etc/ldap/slapd.d -l${LDIFDIR}/eduorg-200210-openldap.ldif
slapadd -n0 -F/etc/ldap/slapd.d -l${LDIFDIR}/psl.ldif
echo "ok"

chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

if [ ! -s /etc/ldap/ssl/slapd.pem -o ! -s /etc/ldap/ssl/slapd.key ]; then
	echo "Pose de certificats SSL par défaut (invalides)"
	mkdir -p /etc/ldap/ssl
	cp -v ${LDIFDIR}/ssl.pem /etc/ldap/ssl/slapd.pem
	cp -v ${LDIFDIR}/ssl.key /etc/ldap/ssl/slapd.key
	chown -R root:openldap /etc/ldap/ssl
	chmod 0755 /etc/ldap/ssl
	chmod 0644 /etc/ldap/ssl/slapd.pem
	chmod 0640 /etc/ldap/ssl/slapd.key
	echo "ok"
fi

${SERVICE} start

echo "Installation de la racine du méta-annuaire (o=meta) .. "
ldapvi --verbose --profile config --ldapmodify --ldapvi --add ${LDIFDIR}/add-meta.ldapvi
echo "ok"

